#!/bin/bash

datadir=`echo $HDFS_CONF_dfs_datanode_data_dir | perl -pe 's#file://##'`
if [ ! -d $datadir ]; then
  echo "Creating datanode data directory: $datadir"
  mkdir --parents $datadir
fi

$HADOOP_PREFIX/bin/hdfs --config $HADOOP_CONF_DIR datanode
